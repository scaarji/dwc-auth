'use strict';
var async = require('async'),
    _ = require('lodash');

var User = require('../models/user');

var PREFIX = 'DWC:';

var UserStore = function(redisClient) {
    this.redisClient = redisClient;
};

UserStore.prototype._getKey = function(login) {
    return PREFIX + 'user:' + login;
};

UserStore.prototype.findByLogin = function(login, callback) {
    var _this = this,
        findUser = function(callback) {
            _this.redisClient.hgetall(_this._getKey(login), callback);
        },
        formatResponse = function(res, callback) {
            callback(null, res ? new User(res) : undefined);
        };

    async.waterfall([
        findUser,
        formatResponse
    ], callback);
};

UserStore.prototype.store = function(user, callback) {
    this.redisClient.hmset(this._getKey(user.login), user.toObject(), function(err) {
        callback(err, user);
    });
};

UserStore.prototype.truncate = function(callback) {
    var _this = this,
        findKeys = function(callback) {
            _this.redisClient.keys(PREFIX + 'user:*', callback);
        },
        removeItems = function(keys, callback) {
            if (!keys || !keys.length) {
                return callback(null);
            }

            _.reduce(keys, function(multi, key) {
                multi.del(key);
                return multi;
            }, _this.redisClient.multi()).exec(function(err) {
                callback(err);
            });
        };

    async.waterfall([
        findKeys,
        removeItems
    ], callback);
};

module.exports = UserStore;
