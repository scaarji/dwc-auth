'use strict';
var async = require('async'),
    _ = require('lodash');

var PREFIX = 'DWC:';

var Token = require('../models/token');

var TokenStore = function(redisClient) {
    this.redisClient = redisClient;
};

TokenStore.prototype._getKey = function(id) {
    return PREFIX + 'token:' + id;
};

TokenStore.prototype.findById = function(id, callback) {
    var _this = this,
        findToken = function(callback) {
            _this.redisClient.hgetall(_this._getKey(id), callback);
        },
        formatResponse = function(res, callback) {
            callback(null, res ? new Token(res) : undefined);
        };

    async.waterfall([
        findToken,
        formatResponse
    ], callback);
};

TokenStore.prototype.store = function(token, callback) {
    var data = token.toObject();
    data.isActive = data.isActive ? 'true' : '';

    this.redisClient.hmset(this._getKey(token.id), data, function(err) {
        callback(err, token);
    });
};

TokenStore.prototype.truncate = function(callback) {
    var _this = this,
        findKeys = function(callback) {
            _this.redisClient.keys(PREFIX + 'token:*', callback);
        },
        removeItems = function(keys, callback) {
            if (!keys || !keys.length) {
                return callback(null);
            }

            _.reduce(keys, function(multi, key) {
                multi.del(key);
                return multi;
            }, _this.redisClient.multi()).exec(function(err) {
                callback(err);
            });
        };

    async.waterfall([
        findKeys,
        removeItems
    ], callback);
};

module.exports = TokenStore;
