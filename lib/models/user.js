'use strict';
var _ = require('lodash'),
    async = require('async'),
    bcrypt = require('bcrypt');

var User = function(data) {
    this.login = String(data.login);
    try {
        bcrypt.getRounds(data.password);
        this.password = data.password;
    } catch (e) {}
};

User.prototype.setPassword = function(password, callback) {
    var _this = this,
        hashPassword = function(callback) {
            bcrypt.hash(password, 10, callback);
        },
        setPassword = function(hash, callback) {
            _this.password = hash;

            callback(null, _this);
        };

    async.waterfall([
        hashPassword,
        setPassword
    ], callback);
};

User.prototype.isPasswordMatch = function(candidate, callback) {
    bcrypt.compare(candidate, this.password, callback);
};

User.prototype.toObject = function() {
    return {
        login: this.login,
        password: this.password
    };
};

module.exports = User;
