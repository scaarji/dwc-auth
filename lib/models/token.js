'use string';
var _ = require('lodash'),
    uuid = require('uuid');

var Token = function(data) {
    var numExpiresAt = parseInt(data.expiresAt);

    this.id = data.id || uuid.v4();
    this.login = String(data.login);
    this.expiresAt = new Date(isNaN(numExpiresAt) ? data.expiresAt : numExpiresAt);
    this.isActive = !!data.isActive;
};

Token.prototype.isValid = function() {
    return this.isActive && this.expiresAt > new Date();
};

Token.prototype.toObject = function() {
    return {
        id: this.id,
        login: this.login,
        expiresAt: this.expiresAt.getTime(),
        isActive: this.isActive
    };
};

Token.create = function(user) {
    return new Token({
        login: user.login,
        expiresAt: Date.now() + 3600000,
        isActive: true
    });
};

module.exports = Token;
