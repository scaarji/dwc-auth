'use strict';
var async = require('async');

var Token = require('../models/token'),
    User = require('../models/user');

var Controller = function(repos) {
    this.repos = repos;
};

/**
 * Tries to authenticate user based on passed credentials
 * @param  {Object}   data
 * @param  {String}   data.login
 * @param  {String}   data.password
 * @param  {Function} callback
 */
Controller.prototype.login = function(data, callback) {
    if (!data.login || !data.password) {
        return process.nextTick(function() {
            callback(new Error('login and password are required'));
        });
    }

    var _this = this,
        findUser = function(callback) {
            _this.repos.users.findByLogin(data.login, callback);
        },
        validatePassword = function(user, callback) {
            if (!user) {
                return callback(new Error('User with login [' + data.login + '] does not exist'));
            }

            user.isPasswordMatch(data.password, function(err, isMatch) {
                callback(err, isMatch && user);
            });
        },
        generateToken = function(user, callback) {
            if (!user) {
                return callback(new Error('Invalid password'));
            }

            var token = Token.create(user);

            _this.repos.tokens.store(token, callback);
        };

    async.waterfall([
        findUser,
        validatePassword,
        generateToken
    ], callback);
};

/**
 * Tries to register user based on passed credentials
 * @param  {Object}   data
 * @param  {String}   data.login
 * @param  {String}   data.password
 * @param  {Function} callback
 */
Controller.prototype.register = function(data, callback) {
    if (!data.login || !data.password) {
        return process.nextTick(function() {
            callback(new Error('login and password are required'));
        });
    }

    var _this = this,
        findUser = function(callback) {
            _this.repos.users.findByLogin(data.login, callback);
        },
        setPassword = function(user, callback) {
            if (user) {
                return callback(new Error('User with login [' + data.login + '] already exists'));
            }

            var newUser = new User({login: data.login});

            newUser.setPassword(data.password, callback);
        },
        saveUser = function(user, callback) {
            _this.repos.users.store(user, callback);
        },
        generateToken = function(user, callback) {
            var token = Token.create(user);

            _this.repos.tokens.store(token, callback);
        };

    async.waterfall([
        findUser,
        setPassword,
        saveUser,
        generateToken
    ], callback);
};

/**
 * Finds and returns token by id
 * @param  {Object}   data
 * @param  {String}   data.id
 * @param  {Function} callback
 */
Controller.prototype.getToken = function(data, callback) {
    if (!data.id) {
        return process.nextTick(function() {
            callback(new Error('id is required'));
        });
    }

    var _this = this,
        findToken = function(callback) {
            _this.repos.tokens.findById(data.id, callback);
        },
        validateToken = function(token, callback) {
            if (!token) {
                return callback(new Error('Token with id [' + data.id + '] not found'));
            }

            if (!token.isValid()) {
                return callback(new Error('Token expired'));
            }

            callback(null, token);
        };

    async.waterfall([
        findToken,
        validateToken
    ], callback);
};

module.exports = Controller;
