'use strict';

var processTokenCallback = function(req, callback) {
    return function(err, token) {
        if (err) {
            callback(err);
        } else {
            callback(null, token.toObject());
        }
    };
};

module.exports = function(controller) {
    return {
        getToken: function(req, callback) {
            controller.getToken(req.params, processTokenCallback(req, callback));
        },
        login: function(req, callback) {
            controller.login(req.params, processTokenCallback(req, callback));
        },
        register: function(req, callback) {
            controller.register(req.params, processTokenCallback(req, callback));
        }
    };
};
