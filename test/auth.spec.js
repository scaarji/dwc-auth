/* global describe, it, before, afterEach */
'use strict';
var async = require('async'),
    should = require('should'),
    redis = require('redis');

var startService = require('../index');

var config = require('./config.json');

var TokensRepo = require('../lib/repositories/tokens'),
    UsersRepo = require('../lib/repositories/users');

describe('Auth Service', function() {
    var service,
        client,
        info;

    var redisClient = redis.createClient(config.redis.port, config.redis.host);
    var repos = {
        tokens: new TokensRepo(redisClient),
        users: new UsersRepo(redisClient)
    };

    before(function(done) {
        service = startService(config, done);
        client = service.client;
        info = service.info();
    });

    var truncate = function(done) {
        async.parallel([
            repos.tokens.truncate.bind(repos.tokens),
            repos.users.truncate.bind(repos.users)
        ], done);
    };

    describe('#register', function() {
        afterEach(truncate);

        it('should return error on empty login', function(done) {
            client.query({
                service: info,
                op: 'register',
                params: {
                    password: 'pass'
                }
            }, function(err, result) {
                should.exist(err);
                err.message.should.match(/login/i);
                should.not.exist(result);

                done();
            });
        });

        it('should return error on empty password', function(done) {
            client.query({
                service: info,
                op: 'register',
                params: {
                    login: 'login'
                }
            }, function(err, result) {
                should.exist(err);
                err.message.should.match(/password/i);
                should.not.exist(result);

                done();
            });
        });

        it('should create user and return token', function(done) {
            var login = 'login';
            client.query({
                service: info,
                op: 'register',
                params: {
                    login: login,
                    password: 'pass'
                }
            }, function(err, result) {
                should.not.exist(err);
                should.exist(result);
                result.should.have.properties(['id', 'login', 'isActive', 'expiresAt']);
                result.login.should.eql(login);
                result.isActive.should.eql(true);

                done();
            });
        });

        it('should return error on duplicate login', function(done) {
            var params = {
                service: info,
                op: 'register',
                params: {
                    login: 'dup-login',
                    password: 'pass'
                }
            };

            async.series([
                function(callback) {
                    client.query(params, callback);
                },
                function(callback) {
                    client.query(params, function(err, result) {
                        should.exist(err);
                        err.message.should.match(/exist/i);
                        should.not.exist(result);

                        callback();
                    });
                }
            ], done);
        });
    });

    describe('#login', function() {
        afterEach(truncate);
        it('should return error on empty login', function(done) {
            client.query({
                service: info,
                op: 'login',
                params: {
                    password: 'pass'
                }
            }, function(err, result) {
                should.exist(err);
                err.message.should.match(/login/i);
                should.not.exist(result);

                done();
            });
        });

        it('should return error on empty password', function(done) {
            client.query({
                service: info,
                op: 'login',
                params: {
                    login: 'login'
                }
            }, function(err, result) {
                should.exist(err);
                err.message.should.match(/password/i);
                should.not.exist(result);

                done();
            });
        });

        it('should return error on non-existent user', function(done) {
            client.query({
                service: info,
                op: 'login',
                params: {
                    login: 'nonexistent-login',
                    password: 'password'
                }
            }, function(err, result) {
                should.exist(err);
                err.message.should.match(/exist/i);
                should.not.exist(result);

                done();
            });
        });

        it('should return error on invalid password', function(done) {
            var user = {
                login: 'login',
                password: 'password'
            };

            async.series([
                function(callback) {
                    client.query({
                        service: info,
                        op: 'register',
                        params: user
                    }, callback);
                },
                function(callback) {
                    client.query({
                        service: info,
                        op: 'login',
                        params: {
                            login: user.login,
                            password: 'invalid-password'
                        }
                    }, function(err, result) {
                        should.exist(err);
                        err.message.should.match(/invalid/i);
                        should.not.exist(result);

                        callback();
                    });
                }
            ], done);
        });

        it('should return token on correct credentials', function(done) {
            var user = {
                login: 'login',
                password: 'password'
            };
            async.series([
                function(callback) {
                    client.query({
                        service: info,
                        op: 'register',
                        params: user
                    }, callback);
                },
                function(callback) {
                    client.query({
                        service: info,
                        op: 'login',
                        params: user
                    }, function(err, result) {
                        should.not.exist(err);
                        should.exist(result);
                        result.should.have.properties(['id', 'login', 'isActive', 'expiresAt']);
                        result.login.should.eql(user.login);
                        result.isActive.should.eql(true);

                        callback();
                    });
                }
            ], done);
        });
    });

    describe('#getToken', function() {
        it('should return error on invalid token', function(done) {
            client.query({
                service: info,
                op: 'getToken',
                params: {
                    id: 'asdfasdf'
                }
            }, function(err) {
                should.exist(err);
                err.message.should.match(/not found/i);

                done();
            });
        });

        it('should return corrent token', function(done) {
            async.waterfall([
                function(callback) {
                    client.query({
                        service: info,
                        op: 'register',
                        params: {
                            login: 'login',
                            password: 'password'
                        }
                    }, callback);
                },
                function(res, callback) {
                    client.query({
                        service: info,
                        op: 'getToken',
                        params: {
                            id: res.id
                        }
                    }, function(err, result) {
                        should.not.exist(err);
                        should.exist(result);
                        result.should.have.properties(['id', 'login', 'isActive', 'expiresAt']);
                        result.id.should.eql(res.id);
                        result.login.should.eql(res.login);
                        result.isActive.should.eql(true);

                        callback();
                    });
                }
            ], done);
        });
    });
});
