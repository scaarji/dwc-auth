# Authorization Service

This service is reponsible for handling users and their access tokens.

## To Be Done

- Update README
- Add tests
- Add validation to models
- Add configuration option for token lifetime
- Add endpoint for token invalidation
- Add garbage collection to remove obsolete tokens
- Add methods to update user data
