#!/usr/bin/env node
'use strict';
var fs = require('fs');

var optimist = require('optimist');
var redis = require('redis');

var Client = require('dwc-client');
var Service = require('dwc-service');

var TokensRepo = require('./lib/repositories/tokens'),
    UsersRepo = require('./lib/repositories/users');

var Controller = require('./lib/controller'),
    createHandlers = require('./lib/handlers');

var startService = module.exports = function(config, callback) {
    var redisClient = redis.createClient(config.redis.port, config.redis.host);
    var handlers = createHandlers(new Controller({
        tokens: new TokensRepo(redisClient),
        users: new UsersRepo(redisClient)
    }));

    var client = new Client(config);
    var service = new Service(client, {name: 'auth', version: 1}, handlers);

    service.register({
        pre: function(message, callback) {
            console.log(new Date(), message.op, JSON.stringify(message.params));
            callback(null, message);
        },
        post: function(data, callback) {
            var message = data.error ?
                data.error.name + ': ' + data.error.message :
                JSON.stringify(data.result);

            console.log(new Date(), message);
            callback(null, data);
        }
    });
    service.start(callback);

    return service;
};

if (!module.parent) {
    var argv = optimist
        .demand('c')
        .alias('c', 'config')
        .describe('c', 'Path to configuration file')
        .argv;

    if (!fs.existsSync(argv.c)) {
        throw new Error('Config [' + argv.c + '] not found');
    }

    var contents = fs.readFileSync(argv.c, {encoding: 'utf8'}),
        config;

    try {
        config = JSON.parse(contents);
    } catch (err) {
        throw new Error('Failed to parse config json, ' + err.message);
    }

    startService(config);
}
